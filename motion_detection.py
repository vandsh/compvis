from SimpleCV import *

disp = Display()
cam = Camera()
threshold = 3.0 # if mean exceeds this amount do something

while disp.isNotDone():
	if disp.mouseLeft:
		break

	previous = cam.getImage() #grab a frame
	time.sleep(0.25) #wait for half a second
	current = cam.getImage() #grab another frame
	diff = current - previous
	matrix = diff.getNumpy()
	mean = matrix.mean()

	diff.show()

	if mean >= threshold:
		print "Motion Detected"