from SimpleCV import *

display = Display()
display2 = Display()
#create the display to show the image
cam = Camera()
# initalize the camera


while display.isNotDone() and display2.isNotDone():
	img = cam.getImage()

	if display.mouseLeft or display2.mouseLeft:
		break
	
	cimg = img
	nimg = cimg.colorDistance(Color.BLACK).dilate(4)
	nimg = nimg.stretch(100,255)
	blobs = nimg.findBlobs()
	# search the image for blob objects
	circles = blobs.filter([b.isCircle(0.5) for b in blobs])
	# filter out only circle shaped blobs

	for b in circles:
		if int(b.radius()) < 60 and int(b.radius()) > 12 :
			#filter out further by radius of found circles
			nimg.drawCircle((b.x, b.y), b.radius(),Color.BLUE,5)
			# draw the circle on the worked on image
			img.drawCircle((b.x, b.y), b.radius(),Color.BLUE,5)
			# draw on the original image
			
			#nimg.save(display)
			# save off the worked on image with circles
			img.save(display2)
			# save off the original image with circles