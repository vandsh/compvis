from SimpleCV import *
cam = Camera()
disp = Display()

while disp.isNotDone():
	img = cam.getImage()
	output = img.edges(t1=160)
	if disp.mouseLeft:
			break
	output.save(disp)